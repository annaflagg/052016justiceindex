This R code compares 2014 state-by-state incarceration rate data to Justice Index data, which is a grade assigned to each state evaluating how good access to civil justice is in that state. Justice Index is a project from the National Center for Access to Justice, just released in 2016. More here: http://justiceindex.org/


To use, run the code in data/script.R
To see the plot without running the code, see data/plots/ji-incarcerationrate.pdf